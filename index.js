/*
* @Author: luck-cui
* @Date:   2017-10-25 19:45:01
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-25 19:55:39
*/
require('babel-core/register');
require('babel-polyfill');
require('./app/index.js');