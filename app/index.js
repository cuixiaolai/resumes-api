/*
* @Author: luck-cui
* @Date:   2017-10-25 19:31:50
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-26 17:37:30
*/


// var net = require('net');
// import './utils/mountModel'
// import './models/model/index'
import router from './routes/index.js';
const Koa = require('koa');
const app = new Koa();
var server = require('http').createServer(app.callback());

// const serve = require('koa-static');
// const views = require('koa-views');

import bodyParser from 'koa-bodyparser'

const kcors = require('kcors');
const session = require("koa-session2");

// import socket from './socket.js'

const log4js = require('log4js')
global.log4 = log4js.getLogger('api');
log4.level = 'debug';
global._ = require('lodash')

import './models/mongodb';
//req time
app.use(async(ctx, next) => {
    const start = new Date();
    await next();
    const ms = new Date() - start;
    log4.info('body', ctx.body)
    log4.info(`${ctx.method} ${ctx.url} - ${ms}ms`);
    log4.info("status", ctx.status)
});




//session
app.use(session({
    key: "SESSIONID", //default "koa:sess"
}));

app.use((ctx, next) => {
    // log4.info('session', ctx.session)
    return next()
})

//cross-domain req



var whitelist = ['http://localhost:3000']


app.use(kcors({
    origin: function(ctx) {
        log4.info(ctx.header,ctx.header.origin)
        if (whitelist.indexOf(ctx.header.origin) !== -1) {
            return ctx.header.origin
        }
    },
    credentials: true,
    allowHeaders: "Origin, X-Requested-With, Content-Type, Accept, If-Modified-Since, X-Custom-Header"
}));



//err
app.use(async(ctx, next) => {
    try {
        await next()
    } catch (err) {
        ctx.body = err.message;
        ctx.status = err.status || 500
    }
})

//body parse

app.use(bodyParser({enableTypes:['json', 'form','text']}));

//package 参数
app.use(async(ctx, next) => {
    ctx.b = _.assign(ctx.request.body, ctx.query)
    await next()
});

// 显示数据
app.use(async(ctx, next) => {
    log4.info('请求参数的body和query', ctx.b)
    await next()

});

//static files
// app.use(serve(__dirname + '/../public'));

//views
// app.use(views(__dirname + '/../public'));



// router
app
    .use(router.routes())
    .use(router.allowedMethods());


//error
//todo errorPage
app.on('error', function(err) {
    log.error('server error', err);
});

app.on('error', function(err, ctx) {
    log.error('server error', err, ctx);
});

server.listen(process.env.PORT || 3001);