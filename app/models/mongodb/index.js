/*
* @Author: luck-cui
* @Date:   2017-10-25 23:17:01
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-26 13:23:06
*/

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var config = require('../../../config/config.js');
var host = config.mongodbConfig.host;
var port = config.mongodbConfig.port;
var username = config.mongodbConfig.username;
var password = config.mongodbConfig.password;
var dbname = config.mongodbConfig.database;
var connectUrl = 'mongodb://' + username+':'+password+'@'+ host + ':'+ port +'/' + dbname;
var options = {
  useMongoClient: true,
  poolSize: 200,
  keepAlive: 100,
  socketTimeoutMS: 0,
  connectTimeoutMS: 0
}
var db = mongoose.connect(connectUrl,options)
		.catch(e=>{
			log4.error("mongoose-connection",e);
			process.exit(1)
		});

var db = mongoose.connection;

db.on('open', function(){ 
  log4.info("mongoose-connection-open",connectUrl);
});

// mongoose.plugin(require('./schema/common/common'));
module.exports =db 

