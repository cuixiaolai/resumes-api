/*
* @Author: luck-cui
* @Date:   2017-10-26 00:38:40
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-26 13:23:10
*/

/**
 *    简历
 *
 * - content 内容
 * - url 地址
 *
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const resumes = new mongoose.Schema({
	content: {
		type:String
	},
	url:{
		type:String
	}
},
{collection:"resumes"});
resumes.plugin(require('./common/common.js'));
export default mongoose.model('resumes', resumes);