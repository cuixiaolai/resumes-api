/*
* @Author: luck-cui
* @Date:   2017-10-26 00:37:04
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-26 11:32:37
*/

/**
 * 所有表公用字段
 *
 * - created_at_ms 创建时间戳
 * - created_at 创建时间
 * - updated_at 更新时间
 * - is_deleted 是否已删除
 * - remark 备注
 */
var moment = require('moment');

module.exports = function (schema, options) {
  schema.add({
    //创建年份
    created_year : {
      type : String,
      require : true
    },
    // 创建月份
    created_month : {
      type : String,
      require : true
    },
    // 创建天
    created_day : {
      type : String,
      require : true
    },
    // 创建小时
    created_hour : {
      type : String,
      require : true
    },
    // 创建分钟
    created_minute : {
      type : String,
      require : true
    },
    
    // 创建时间戳
    created_at_ms: {
      type: String
    },
    // 创建时间
    created_time: {
      type: Date,
      default: Date.now
    },
    // 最后修改时间
    update_time: {
      type: Date,
      default: Date.now
    },
    // 是否已删除
    is_deleted: {
      type: Boolean,
      default: false
    },
    // 备注
    remark: {
      type: String
    },
  })
  
  schema.pre('save', function (next) {
    var date = moment().format('YYYY-MM-DD-hh-mm').split('-');
    this.created_at_ms  = new Date().getTime();
    this.update_time  = Date.now;
    this.created_year   = date[0];
    this.created_month  = date[1];
    this.created_day    = date[2];
    this.created_hour   = date[3];
    this.created_minute = date[4];
    next();
  })
  
  schema.pre('findOneAndUpdate', function () {
  	this.update({},{ $set: { update_time: new Date() } });
  })
  
  schema.pre('update', function () {
  	this.update({},{ $set: { update_time: new Date() } });
  })

}

