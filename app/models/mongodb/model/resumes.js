/*
* @Author: luck-cui
* @Date:   2017-10-26 00:42:53
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-26 01:09:12
*/

import resumes from '../schema/resumes';

export default {
  delete (opts) {
    return resumes.findOneAndUpdate({'_id':opts.id || opts._id}, { 'is_deleted': true})
  },
  update (opts) {
    if(opts.$query)
      return resumes.update(opts.$query, opts.$data,{multi: true,upsert:true})
    return resumes.findOneAndUpdate({'_id':opts.id || opts._id}, opts,{new:true,upsert:true})
  },
  select (...arg) {
    return (arg.length === 0) ? resumes.find({is_deleted: false}) :  (() => resumes.find(arg[0]))(arg[0]['is_deleted']=false)   ///
  },
  insert (opts) {
    return resumes.create(opts)
  }
}