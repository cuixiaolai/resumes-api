/*
* @Author: luck-cui
* @Date:   2017-10-25 20:14:26
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-26 13:28:54
*/
import resumes from '../models/mongodb/model/resumes.js';

export default {
  async delete (ctx) {
    ctx.body = await resumes.delete(ctx.b)
  },
  async update (ctx) {
    ctx.body = await resumes.update(ctx.b)
  },
  async select (ctx) {
    ctx.body = await resumes.select(ctx.b)
  },
  async insert (ctx) {
    ctx.body = await resumes.insert(ctx.b)
  }
}