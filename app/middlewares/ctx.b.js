/*
* @Author: luck-cui
* @Date:   2017-10-25 20:44:30
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-26 13:37:39
*/
export default 
  async function packageParams (ctx,next) {
  	ctx.b = _.assign(ctx.b, ctx.params)
  	log4.info('请求参数的body和query和params', ctx.b)
    await next()
  }
