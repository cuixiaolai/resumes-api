/*
* @Author: luck-cui
* @Date:   2017-10-25 19:39:28
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-26 13:32:11
*/

import fs from 'fs';
import path from 'path';
import Router from 'koa-router';


const basename = path.basename(module.filename);
const router = Router({
    prefix: '/api/v1'
});
// router.use(async (ctx,next)=>{ctx.b = _.assign(ctx.b, ctx.params);console.log("cuiii",ctx.b);await next();})

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(function(file) {
    let route = require(path.join(__dirname, file));
    router
    .use(route.routes())
    .use(route.allowedMethods())
    ;
  });
router.get('/', (ctx)=>{
	ctx.body = "ssssscui"
});

export default router;
