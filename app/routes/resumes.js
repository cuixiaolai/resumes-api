/*
* @Author: luck-cui
* @Date:   2017-10-25 20:11:25
* @Last Modified by:   luck-cui
* @Last Modified time: 2017-10-26 13:35:33
*/

import Router from 'koa-router';
import packageParams from '../middlewares/ctx.b.js'
import resumes from '../controllers/resumes.js'

const router = Router();

router.get('/resumes/:url', packageParams,resumes.select);
router.get('/resumes/:page/:sum', packageParams,resumes.select);
router.post('/resumes', resumes.insert);
router.put('/resumes', resumes.update);

module.exports = router;